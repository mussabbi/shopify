void print_intro();
void log_in(char *username, char *password);
void main_menu();
void sell_buy_menu(char *sell_buy_option);
void show_artwork(int first_artwork, int last_artwork);
int verify_user(char *username, char *password);
int manage_inventory();
