#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_intro() {

  system("clear");

  FILE *fptr;

    char c;

    /* Open file */
    fptr = fopen("intro.txt", "r");
    if (fptr == NULL)
    {
        printf("Cannot open file \n");
        exit(0);
    }

    /* Read contents from file */
    c = fgetc(fptr);
    while (c != EOF)
    {
        printf ("%c", c);
        c = fgetc(fptr);
    }

    fclose(fptr);

}

void log_in(char *username, char *password) { /*Logging in mechanism*/

  system("clear");

  printf("Please enter your username: ");

  scanf("%s", username);
  fflush(stdin);

  system("clear");

  printf("Please enter your password: ");

  scanf("%s", password);
  fflush(stdin);

}

int verify_user(char *username, char *password) {

  char name[10] = "user";
  char pass[10] = "123";

  if(strcmp(name, username) == 0 && strcmp(pass, password) == 0) { /*case where our log in is successful*/

    system("clear");
    printf("Welcome User!\n");
    return(1);

  }

  system("clear");
  printf("Log in failed - Cannot find user!\n");

  exit(0);

}

void main_menu(char *main_menu_option) {
/*prompts user to select an option from the main manu and puts it in the pointer*/
  printf("\n[1] Search (under maintainence!)\n[2] Add images (under maintainence!)\n[3] Delete (under maintainence!)\n[4] Sell/Buy\n");
  printf("\nEnter a number to select an option: ");

  getc(stdin);
  scanf("%c", main_menu_option);
  fflush(stdin);

}

void sell_buy_menu(char *sell_buy_option) {
/*Prompts users to select an option from the buy/sell menu and puts it in the pointer*/
  system("clear");

  printf("Sell/Buy menu\n");
  printf("\n[1] Manage inventory\n[2] Apply discount\n");
  printf("\nEnter a number to select an option: ");

  getc(stdin);
  fflush(stdin);
  scanf("%c", sell_buy_option);
  fflush(stdin);

}

void show_artwork(int first_artwork, int last_artwork) {

  char c;
  FILE *fptr;

if(first_artwork == 1) {

    fptr = fopen("artwork1.txt", "r");

    c = fgetc(fptr);
    while (c != EOF) {
        printf ("%c", c);
        c = fgetc(fptr);
    }

    fclose(fptr);

    printf("\n\n");

}

if(last_artwork == 2) {

    fptr = fopen("artwork2.txt", "r");

    c = fgetc(fptr);
    while (c != EOF) {
        printf ("%c", c);
        c = fgetc(fptr);
    }

    fclose(fptr);

    printf("\n\n");

}

}

int manage_inventory() {

  char wallet[10]; /*Holds the temp value from the file*/
  char item_to_sell[1];
  int cash; /*actual int value we can work with*/
  int first_artwork = 1; /*Access to artwork*/
  int last_artwork = 2;
  FILE *fptr;

  system("clear");

  fptr = fopen("wallet.txt", "r+");

  if (fptr == NULL) { /*Error checking if file isnt present*/
      printf("Cannot open file \n");
      exit(0);
  }

  fgets(wallet, 10, fptr);
  fclose(fptr);

  sscanf(wallet, "%d", &cash);
  printf("Amount in wallet: %d\n\n", cash);

  printf("List of artwork: \n\n");
  show_artwork(first_artwork, last_artwork);

  printf("Enter artwork number to sell [enter 0 if you dont want to sell]: ");

  getc(stdin);
  scanf("%c", item_to_sell);
  fflush(stdin);

  if(item_to_sell[0] == '0') {

    printf("Thank you for using Jpeg Depo!\n");
    return(1);

  }

  if(item_to_sell[0] == '1') {
    system("clear");
    first_artwork = 0;
    cash = cash + 100;

    printf("Amount in wallet: %d\n\n", cash);

    printf("List of artwork: \n\n");
    show_artwork(first_artwork, last_artwork);

    fptr = fopen("wallet.txt", "w"); /*updating wallet amount in file*/
    fprintf(fptr, "%d", cash);
    fclose(fptr);
  }

  if(item_to_sell[0] == '2') {
    system("clear");
    last_artwork = 0;
    cash = cash + 40;

    printf("Amount in wallet: %d\n\n", cash);

    printf("List of artwork: \n\n");
    show_artwork(first_artwork, last_artwork);

    fptr = fopen("wallet.txt", "w"); /*updating wallet amoint in file*/
    fprintf(fptr, "%d", cash);
    fclose(fptr);
  }

  return(1);
}
