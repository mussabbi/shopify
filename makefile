CC = gcc
CFLAGS = -ansi -Wall

app: main.c functions.o
	$(CC) $(CFLAGS) main.c functions.o -o app

functions.o: functions.c shopifylib.h
	$(CC) $(CFLAGS) -c functions.c -o functions.o

clean:
	rm app functions.o
